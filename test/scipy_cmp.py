import numpy as np
import scipy.optimize

# Do the same minimization as test.cpp but using the Fortran library through scipy.

f0 = 7
g = np.array([-2.0,3.0])
H = np.array([[7.0, 1.0],
              [1.0, 4.0]])
x0 = np.array([0.4, 0.6])

def fun(x):
    print "fun(",x,")"
    return f0 + np.inner(g,x) + 0.5*np.inner(x,np.dot(H,x))

def jac(x):
    return g + np.dot(H,x)

res = scipy.optimize.minimize(fun, x0, method='L-BFGS-B', jac=jac, bounds = [(0,100),(0,100)])
