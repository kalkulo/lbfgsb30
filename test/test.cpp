#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include "lbfgsb30.h"

int main()
{
  lbfgsb30_job job(2,5);
  job.x[0] = 0.4; job.x[1] = 0.6;
  job.set_bounds(0,0,100);
  job.set_bounds(1,0,100);
  job.step();
  // f(x) = f0 + g'x + 1/2xHx
  // grad f(x) = g + Hx  
  double f0 = 7;
  std::vector<double> g{-2,3};
  std::vector<double> H{7,1,1,4};
  int k = 0;
  while (job.task == LBFGSB30_FG ||
	 job.task == LBFGSB30_FG_START ||
	 job.task == LBFGSB30_FG_LNSRCH ||
	 job.task == LBFGSB30_NEW_X)
    {
      if (job.task != LBFGSB30_NEW_X)
	{
	  std::vector<double> Hx = {job.x[0]*H[0] + job.x[1]*H[1],
				    job.x[0]*H[2] + job.x[1]*H[3]};      
	  job.f = f0 + job.x[0]*g[0] + job.x[1]*g[1] + 0.5*(job.x[0]*Hx[0] + job.x[1]*Hx[1]);
	  job.g[0] = g[0] + Hx[0];
	  job.g[1] = g[1] + Hx[1];
	  std::cout << "Iter " << k << " x " << job.x[0] << " " << job.x[1] << " gradnorm " << sqrt((job.g[0]*job.g[0] + job.g[1]*job.g[1])) << std::endl;
	}
      else
	{
	}
      job.step();
      k++;
    }
  std::cout << "Finished. Final x: ";
  for (int i=0;i<job.n;i++)
    std::cout << job.x[i] << " ";
  std::cout << std::endl;
  return 0;
}

