#!/bin/sh

SRC=Lbfgsb.3.0
# Concatenate all Fortran files to one (to allow f2c to do a better job)
cat $SRC/lbfgsb.f $SRC/blas.f $SRC/linpack.f $SRC/timer.f > all.f
# Apply minor fixes and annotate the subroutines/functions in the Fortran files
patch all.f pre_f2c.patch
# Run f2c
f2c -a all.f
### Clean up C file (this patch depends on the f2c version)
# Mark functions as static
sed -e 's/\/\* tag-subroutine \*\//static/g' < all.c > tmp.c
# Apply manual fixes
