static int mainlb_(integer *, integer *, doublereal *, 
		   doublereal *, doublereal *, integer *, doublereal *, doublereal *,
		   doublereal *, doublereal *, doublereal *, doublereal *, 
		   doublereal *, doublereal *, doublereal *, doublereal *, 
		   doublereal *, doublereal *, doublereal *, doublereal *, 
		   doublereal *, doublereal *, doublereal *, integer *, integer *, 
		   integer *, char *, integer *, char *, logical *, integer *, 
		   doublereal *, ftnlen, ftnlen);

static doublereal ddot_(integer *, doublereal *, integer *, doublereal *, 
			integer *);

static int dscal_(integer *, doublereal *, doublereal *, integer *);

static int freev_(integer *, integer *, integer *, 
				   integer *, integer *, integer *, integer *, logical *, logical *, 
				   logical *, integer *, integer *);
static int dcopy_(integer *, doublereal *, 
		  integer *, doublereal *, integer *);

static int timer_(doublereal *);
static int formk_(integer *, 
		  integer *, integer *, integer *, integer *, integer *, integer *, 
		  logical *, doublereal *, doublereal *, integer *, doublereal *, 
		  doublereal *, doublereal *, doublereal *, integer *, integer *, 
		  integer *);

static
int formt_(integer *m, doublereal *wt, doublereal *sy, 
	   doublereal *ss, integer *col, doublereal *theta, integer *info);

static
int subsm_(integer *n, integer *m, integer *nsub, integer *
	ind, doublereal *l, doublereal *u, integer *nbd, doublereal *x, 
	doublereal *d__, doublereal *xp, doublereal *ws, doublereal *wy, 
	doublereal *theta, doublereal *xx, doublereal *gg, integer *col, 
	integer *head, integer *iword, doublereal *wv, doublereal *wn, 
			    integer *iprint, integer *info);


static int prn1lb_(integer *, integer *, doublereal *, 
				    doublereal *, doublereal *, integer *, integer *, doublereal *);

static
int prn2lb_(integer *, doublereal *, doublereal *, doublereal *, 
	    integer *, integer *, integer *, integer *, integer *, doublereal 
	    *, integer *, char *, integer *, integer *, doublereal *, 
	    doublereal *, ftnlen);
static
int prn3lb_(integer *, doublereal *, 
	    doublereal *, char *, integer *, integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *, doublereal *, 
	    doublereal *, integer *, char *, integer *, doublereal *, 
	    doublereal *, integer *, doublereal *, doublereal *, doublereal *,
	    ftnlen, ftnlen);

static
int errclb_(integer *, integer *, doublereal *, 
	    doublereal *, doublereal *, integer *, char *, integer *, integer 
	    *, ftnlen);
static
int active_(integer *, doublereal *, doublereal *,
	    integer *, doublereal *, integer *, integer *, logical *, 
	    logical *, logical *);

static
int cauchy_(integer *, doublereal *, 
	    doublereal *, doublereal *, integer *, doublereal *, integer *, 
	    integer *, doublereal *, doublereal *, doublereal *, integer *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, integer *, integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *, integer *, doublereal *, 
	    integer *, doublereal *);

static int cmprlb_(integer *, integer *, doublereal *, 
		   doublereal *, doublereal *, doublereal *, doublereal *, 
		   doublereal *, doublereal *, doublereal *, doublereal *, integer *,
		   doublereal *, integer *, integer *, integer *, logical *, 
		   integer *);

static
int matupd_(integer *, integer *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, integer *, integer *, integer *, integer *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *);

static
int lnsrlb_(integer *, doublereal *, doublereal *,
	    integer *, doublereal *, doublereal *, doublereal *, doublereal *
	    , doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, integer *, integer *, 
	    integer *, integer *, integer *, char *, logical *, logical *, 
	    char *, integer *, doublereal *, ftnlen, ftnlen);

static
int projgr_(integer *, doublereal *, doublereal *,
	    integer *, doublereal *, doublereal *, doublereal *);

static
int dtrsl_(doublereal *, integer *, integer *, 
	   doublereal *, integer *, integer *);

static int bmv_(integer *, doublereal *, doublereal *, 
      	 integer *, doublereal *, doublereal *, integer *);

static int dcopy_(integer *, doublereal *, integer *, 
		  doublereal *, integer *);

static int daxpy_(integer *, doublereal *, 
		  doublereal *, integer *, doublereal *, integer *);
static int hpsolb_(integer *, doublereal *, integer *, 
		   integer *);
static int dpofa_(doublereal *, integer *, integer *, 
				   integer *);
static int dcopy_(integer *, doublereal *, integer *, doublereal 
		  *, integer *);
static int dtrsl_(doublereal *, integer *, integer *, 
		  doublereal *, integer *, integer *);

static int dcsrch_(doublereal *, doublereal *, 
		   doublereal *, doublereal *, doublereal *, doublereal *, 
		   doublereal *, doublereal *, char *, integer *, doublereal *, 
		   ftnlen);

static int dcstep_(doublereal *, doublereal *, 
		   doublereal *, doublereal *, doublereal *, doublereal *, 
		   doublereal *, doublereal *, doublereal *, logical *, doublereal *,
		   doublereal *);

static int cpu_time__(real *);
