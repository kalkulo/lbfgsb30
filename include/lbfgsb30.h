#ifndef LBFGSB30_H
#define LBFGSB30_H

#ifdef __cplusplus
#include <memory>
#include <cassert>

extern "C" {
#endif

enum lbfgsb30_task
  {
   LBFGSB30_START = 0,
   LBFGSB30_NEW_X,
   LBFGSB30_FG,
   LBFGSB30_FG_START,
   LBFGSB30_FG_LNSRCH,
   LBFGSB30_CONVERGENCE,
   LBFGSB30_STOP,
   LBFGSB30_ABNORMAL_TERMINATION_IN_LNSRCH,   
   LBFGSB30_FIRST_WARNING,
   LBFGSB30_WARNING_ROUNDING_ERRORS_PREVENT_PROGRESS,
   LBFGSB30_WARNING_XTOL_TEST_SATISFIED,
   LBFGSB30_WARNING_STP_EQ_STPMAX,
   LBFGSB30_FIRST_ERROR,
   LBFGSB30_ERROR_N_LE_0,
   LBFGSB30_ERROR_M_LE_0,
   LBFGSB30_ERROR_FACTR_LT_0,
   LBFGSB30_ERROR_INVALID_NBD,
   LBFGSB30_ERROR_NO_FEASIBLE_SOLUTION,
   LBFGSB30_ERROR_STP_LT_STPMIN,
   LBFGSB30_ERROR_STP_GT_STPMAX,
   LBFGSB30_ERROR_INITIAL_G_GE_ZERO,
   LBFGSB30_ERROR_FTOL_LT_ZERO,
   LBFGSB30_ERROR_GTOL_LT_ZERO,
   LBFGSB30_ERROR_XTOL_LT_ZERO,
   LBFGSB30_ERROR_STPMIN_LT_ZERO,
   LBFGSB30_ERROR_STPMAX_LT_STPMIN,
   LBFGSB30_ERROR_TASK, // task had an illegal value on input
  };

  /* This is a lightly wrapped version of the setulb subroutine from
     L-BFGS-B 3.0. See the library documentation for interface. The 
     task string has been replaced by an enum to hide the Fortran
     string manipulations. I/O has been disabled, corresponding
     to iprint=-1 in the original routine. Timings inside
     the code have also been disabled, and are not correct in the output.*/
void lbfgsb30_setulb(int n,  int m, double *x, double *l, double *u,
		     int *nbd, double *f, double *g, double factr, double pgtol,
		     double *wa, int *iwa, enum lbfgsb30_task *task, char csave[60], int lsave[4],
		     int isave[44], double dsave[29]);

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
/* 
   Book-keeping class for interfacing with L-BFGS-B from C++.
*/
class lbfgsb30_job
{
 public:
  lbfgsb30_job(int n, int m = 5) : x(new double[n]), nbd(new int[n]),
				   l(new double[n]), u(new double[n]), g(new double[n]),
				   wa(new double[(2*m + 5)*n + 12*m*m + 12*m]),  iwa(new int[3*n])
    {
      this->n = n;
      this->m = std::min(m, n);  // m must be limited by n, otherwise
      // wa will not be initialized correctly and garbage values may
      // enter the computations
    }
  enum lbfgsb30_task task = LBFGSB30_START;
  int n, m;
  double f;
  double factr = 1e7; /* 1e12 for low, 1e7 for moderate and 1e1 for extremely high accuracy */
  double pgtol = 1e-5;
  std::unique_ptr<double[]>  x; // Current iterate
  std::unique_ptr<int[]>   nbd; // Bounds type (0 unbounded, 1 lb, 2 lb&ub, 3 ub)
  std::unique_ptr<double[]>  l; // Lower bounds (if active)
  std::unique_ptr<double[]>  u; // Upper bounds (if active)
  std::unique_ptr<double[]>  g; // Gradient at x
  std::unique_ptr<double[]>  wa; // Internal work array
  std::unique_ptr<int[]>    iwa; // Internal work array
  double dsave[29];
  int lsave[4], isave[44];
  char csave[60];
  void step()
  {
    lbfgsb30_setulb(n,  m, &x[0], &l[0], &u[0],
		    &nbd[0], &f, &g[0], factr, pgtol,
		    &wa[0], &iwa[0], &task, csave, lsave, isave, dsave);
  }
  // Set bounds on variable i. Use NAN to disable a bound.
  void set_bounds(int i, double lb, double ub)
  {
    assert(i >= 0);
    assert(i < n);
    l[i] = lb;
    u[i] = ub;
    if (std::isnan(lb))
      {
	if (std::isnan(ub))
	  nbd[i] = 0;
	else
	  nbd[i] = 3;
      }
    else
      {
	if (std::isnan(ub))
	  nbd[i] = 1;
	else
	  nbd[i] = 2;
      }
  }
  int nr_feval() const
  {
    assert(task == LBFGSB30_NEW_X);
    return isave[34];
  }
  /* The infinity norm of the projected gradient. */
  double grad_norm() const
  {
    assert(task == LBFGSB30_NEW_X);
    return dsave[12];
  }
  /* The theta scale factor representing the diagonal part of the Hessian */
  double theta() const
  {
    return dsave[0];
  }
  /* Rough error estimate for the error in function value */
  double fun_error_estimate() const
  {
    return 0.5*grad_norm()*grad_norm()/theta();
  }
  /* Rough estimate for the maximum error in x */
  double x_error_estimate() const
  {
    return grad_norm()/theta();
  }
  /* Return a rigorous (positive) upper bound of the error in function value,
     valid for convex functions and active bounds. 

     A convex function is guaranteed to be greater than f - err, were err 
     is the return value of this function. The estimate is based on the 
     position x and the gradient g at that point.
  */
  double fun_convex_error_bound() const
  {
    double err = 0;
    for (int i=0;i<n;i++)
      {
	if (nbd[i] == 1 && g[i] >= 0)
	  err += g[i]*(l[i] - x[i]);
	else if (nbd[i] == 3 && g[i] <= 0)
	  err += g[i]*(u[i] - x[i]);
	else if (nbd[i] == 2)
	  err += std::min(g[i]*(u[i] - x[i]), g[i]*(l[i] - x[i]));
	else
	  err += INFINITY;
      }
    return -err;
  }
};

#endif

#endif
