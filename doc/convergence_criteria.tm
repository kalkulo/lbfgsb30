<TeXmacs|1.99.5>

<style|generic>

<\body>
  Let <math|x<rsub|0>> be the point we are considering to stop at and let
  <math|x<rsub|\<ast\>>> be the unknown true minimum in the box. The box has
  sides <math|d<rsub|i>=ub<rsub|i>-lb<rsub|i>>.

  When the Hessian exists we have the relation

  <\equation>
    \<nabla\>f<around*|(|x<rsub|0>|)>=H<around*|(|x<rsub|\<ast\>>|)><around*|(|x<rsub|0>-x<rsub|\<ast\>>|)>+\<ldots\>
  </equation>

  very roughly then

  <\equation>
    \<\|\|\>x<rsub|0>-x<rsub|\<ast\>>\<\|\|\>\<approx\><frac|\<\|\|\>\<nabla\>f<around*|(|x<rsub|0>|)>\<\|\|\>|<around*|\<\|\|\>|H<around*|(|x<rsub|0>|)>|\<\|\|\>>>,
  </equation>

  where we can use some scalar approximation of <math|H> (maybe
  '<math|\<theta\>>' from L-BFGS-B). This gives us some indication of the
  error in <math|x<rsub|0>>. Here it is appropriate to replace
  <math|\<nabla\>f<around*|(|x<rsub|0>|)>> by the projected gradient from
  L-BFGS-B. Similarly we can estimate the error in the function value

  <\equation>
    f<around*|(|x<rsub|\<ast\>>|)>=f<around*|(|x<rsub|0>+\<delta\>|)>=f<around*|(|x<rsub|0>|)>+\<nabla\>f<around*|(|x<rsub|0>|)><rsup|T>\<delta\>+<frac|1|2>\<delta\><rsup|T>H\<delta\>+\<ldots\>
  </equation>

  <\equation>
    <around*|\||f<around*|(|x<rsub|\<ast\>>|)>-f<around*|(|x<rsub|0>|)>|\|>\<approx\><frac|1|2><frac|<around|\<\|\|\>|\<nabla\>f<around*|(|x<rsub|0>|)>|\<\|\|\>><rsup|2>|<around*|\<\|\|\>|H<around*|(|x<rsub|0>|)>|\<\|\|\>>>
  </equation>

  where we have used that <math|\<delta\>\<approx\>-\<nabla\>f<around*|(|x<rsub|0>|)>/\<\|\|\>H\<\|\|\>>.

  For <with|font-shape|italic|convex> functions we can rigorously bound the
  error in the function value as

  <\equation>
    f<around*|(|x<rsub|\<ast\>>|)>\<geqslant\>f<around*|(|x<rsub|0>|)>+\<nabla\>f<around*|(|x<rsub|0>|)><rsup|T><around*|(|x<rsub|\<ast\>>-x<rsub|0>|)>
  </equation>

  which can be minimized over the box as

  <\equation>
    f<around*|(|x<rsub|\<ast\>>|)>\<geqslant\>f<around*|(|x<rsub|0>|)>+<big|sum><rsub|i>min<around*|{|\<nabla\>f<around*|(|x<rsub|0>|)><rsub|i><around*|(|ub<rsub|i>-x<rsub|0i>|)>,\<nabla\>f<around*|(|x<rsub|0>|)><rsub|i><around*|(|lb<rsub|i>-x<rsub|0i>|)>|}>.
  </equation>

  where we have to use the full (not projected) gradient.\ 

  If we accept errors <math|<left|\|>f<around*|(|x<rsub|0>|)>-f<around*|(|x<rsub|\<ast\>>|)><left|\|>\<leqslant\>\<epsilon\><rsub|f>>
  and/or <math|\<\|\|\>x<rsub|0>-x<rsub|\<ast\>>\<\|\|\>\<leqslant\>\<epsilon\><rsub|x>>,
  and if we use the infinity norm of the projected gradient <math|G> and the
  L-BFGS-B <math|\<theta\>> value as an approximation to the curvature, we
  get

  <\equation>
    <frac|1|2><frac|G<rsup|2>|\<theta\>>\<leqslant\>\<epsilon\><rsub|f>
  </equation>

  and

  <\equation>
    <frac|G|\<theta\>>\<leqslant\>\<epsilon\><rsub|x>.
  </equation>

  For a quadratic <math|a x<rsup|2>> we get

  <\equation>
    f<around*|(|x|)>-f<around*|(|0|)>\<approx\><frac|1|2><frac|4a<rsup|2>
    x<rsup|2>|2a>=a<rsup|2>x<rsup|2>
  </equation>

  <\equation>
    x\<approx\><frac|2a x| 2a>=x
  </equation>

  Alternatively we can assume a convex function and use the then-rigorous
  requirement

  <\equation>
    <around*|\||<big|sum><rsub|i>min<around*|{|\<nabla\>f<around*|(|x<rsub|0>|)><rsub|i><around*|(|ub<rsub|i>-x<rsub|0i>|)>,\<nabla\>f<around*|(|x<rsub|0>|)><rsub|i><around*|(|lb<rsub|i>-x<rsub|0i>|)>|}>|\|>\<leqslant\>\<epsilon\><rsub|f>.
  </equation>
</body>

<initial|<\collection>
</collection>>